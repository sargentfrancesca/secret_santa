from django.shortcuts import render
from santa.models import UserGiftee

def index(request):
    user = request.user if request.user.is_authenticated else None
    match = UserGiftee.objects.filter(giver=user).first()

    context = {
        'user': user,
        'match': match,
    }

    return render(request, "index.html", context)
