from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()

class UserGiftee(models.Model):
    giver = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="buying_for"
    )
    receiver = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="bought_for_by"
    )

    def __str__(self):
        return f"{self.giver} is buying for {self.receiver}"
