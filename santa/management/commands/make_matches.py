import random
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string

from santa.models import UserGiftee

class Command(BaseCommand):
    help = 'Make secret santa matches'

    def add_arguments(self, parser):
        parser.add_argument('-f', '--flush', action='store_true', help='Remove and rematch', )

    def handle(self, *args, **kwargs):
        flush = kwargs['flush']

        users = User.objects.all()
        matches = UserGiftee.objects.all()

        if flush:
            matches.delete()

        if matches:
            self.stdout.write(self.style.ERROR('Matches exist already, exiting'))
            return

        

        for user in users:
            users_already_matched = UserGiftee.objects.values_list('receiver__id', flat=True)
            available_users = User.objects.exclude(id=user.id).exclude(id__in=users_already_matched)
            
            random_person = random.choice(available_users)
            
            new_match = UserGiftee(
                giver=user,
                receiver=random_person
            )
            new_match.save()

            self.stdout.write(self.style.SUCCESS(new_match))

            
